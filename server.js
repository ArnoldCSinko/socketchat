const app = require('express')();
const server = require('http').Server(app);
const io = require('socket.io')(server);


io.on('connection', (socket) => {

    socket.on('disconnect', () => {
        console.log('socket disconnect...', socket.id)
        handleDisconnect()
    })

    socket.on('error', (err) => {
        console.log('received error from socket:', socket.id)
        console.log(err)
    })
});

const PORT = 3000;
server.listen(PORT, (err) => {
    if (err) throw err
    console.log(`Server running on port ${PORT}`)
});